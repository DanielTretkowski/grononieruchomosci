<?php

namespace Sda\grononieruchomosci\Controller;
use sda\grononieruchomosci\Request\Request;
use Sda\grononieruchomosci\Response\Response;


class NieruchomosciController
{
 
    /**
     * @var Request
     */
    private $request;
    /**
     * @var Response
     */
    private $response;

    /**
     * @param Request $request
     * @param Response $response
     */
    public function __construct(
        Request $request,
        Response $response
    ){
        $this->request=$request;
        $this->response=$response;
    }

    public function run(){

        $action = $this->request->getParamFromGet('action', 'index');

        switch ($action){
            case 'index':
                require_once __DIR__ . '/../../httpdocs/index.html';
                break;
            case 'mail':
                
                    $from = $this->request->getParamFromPost('name');
                    $msg = $this->request->getParamFromPost('tekst');
                    $mail = $this->request->getParamFromPost('email');
                    $this->response->sendMail($from, $msg, $mail);
                break;
            default :
                require_once __DIR__ . '/../../httpdocs/404.html';
                break;
        }


        
    }
}