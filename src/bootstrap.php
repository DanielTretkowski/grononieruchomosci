<?php


require_once __DIR__ . '/../vendor/autoload.php';

use sda\grononieruchomosci\Controller\NieruchomosciController;
use sda\grononieruchomosci\Request\Request;
use sda\grononieruchomosci\Response\Response;


$request = new Request();
$response = new Response();

$app = new NieruchomosciController($request, $response);
$app->run();