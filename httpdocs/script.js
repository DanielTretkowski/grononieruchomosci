$(document).ready(function () {


    $(function () {
        var offset = -100;
        var scrollTime = 500;
        $('a[href^="#"]').click(function () {
            $(this).parent().siblings().removeClass("active");
            $(this).parent().addClass("active");
            $("html, body").animate({
                scrollTop: $($(this).attr("href")).offset().top + offset
            }, scrollTime);
            return false;
        });

    });

    $(function () {

        $('#subButton').click(function (e) {

            var data = {};
            $(this).parents('form').children().find('input, textarea').not('[type="submit"]').each(function () {
                data[$(this).attr('name')] = $(this).val();
            });

            $.ajax({
                type: "POST",
                url: "?action=mail",
                data: data,
                success: function () {

                    $('#contactForm').fadeOut(1000, function () {
                        $('#replace').fadeIn(1000);
                    });
                }
            });

            e.preventDefault();
        });
    });
});


function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 54.518048, lng: 18.541724},
        zoom: 17
    });

    var infowindow = new google.maps.InfoWindow();
    var service = new google.maps.places.PlacesService(map);

    service.getDetails({
        placeId: 'ChIJcc81yDqn_UYR_oRkHUmy8dQ'
    }, function (place, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
            var marker = new google.maps.Marker({
                map: map,
                position: {lat: 54.518048, lng: 18.541724}
            });
            google.maps.event.addListener(marker, 'click', function () {
                infowindow.setContent('<div id="content">' +
                    '<div id="siteNotice">' +
                    '</div>' +
                    '<h1 id="firstHeading" class="firstHeading">Grono Nieruchomosci</h1>' +
                    '<IMG width=180 BORDER="0" SRC="img/logo.png">' +
                    '<div id="bodyContent">' + '<p>' + place.formatted_address + '</br>' +
                    'Znajdujemy sie nad biedronka -> <a target="_blank" href ="http://www.google.com/maps/dir/current+position/swietojanska36">Wskazowki dojazdu</a></p>' +
                    '</div>' +
                    '</div>'
                );
                infowindow.open(map, this);
            });
        }
    });
}